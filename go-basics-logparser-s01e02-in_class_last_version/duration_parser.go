package logparser

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

func DurationParser(dur string, start time.Time, end time.Time) (time.Time, error) {
	if dur != "0,s" {
		d := strings.Split(dur, ",")

		value, err := strconv.Atoi(d[0])
		if err != nil {
			fmt.Println("Error while converting to int:", err)
			os.Exit(-1)
		}

		switch d[1] {
		case "s":
			fmt.Println(start.Add(time.Second * time.Duration(value)))
			return start.Add(time.Second * time.Duration(value)), nil
		case "m":
			fmt.Println(start.Add(time.Minute * time.Duration(value)))
			return start.Add(time.Minute * time.Duration(value)), nil
		case "h":
			fmt.Println(start.Add(time.Hour * time.Duration(value)))
			return start.Add(time.Hour * time.Duration(value)), nil
		default:
			fmt.Println("Invalid time mesurment")
			os.Exit(-1)
		}
	} else {
		return end, nil
	}
	return end, nil
}

package main

import (
	"flag"
	"fmt"
	"logparser"
	"os"
	"time"
)

type jsonLogLine struct {
	Ts string `json:"ts"`
}

func main() {

	startStringRef := flag.String("start", "2019-11-13 17:00:00.000", "The start time to look for in logs")
	endStringRef := flag.String("end", "2019-11-13 18:00:00.000", "The end time to look for in logs")
	durationRef := flag.String("dur", "0,s", "The duration from start")

	flag.Parse()

	if startStringRef == nil || endStringRef == nil {
		fmt.Println("Missing start and / or end time")
		os.Exit(-1)
	}

	parser := logparser.CombinedParser{
		Parsers: []logparser.LogFileParser{
			//&logparser.HttpAccessLineParser{"/Users/varadiadam/Downloads/logs/httpd/access.*.log"},
			//&logparser.JsonLogLineParser{"/Users/varadiadam/Downloads/logs/vizqlserver/nativeapi_vizqlserver*.txt"},

			//for easy testing
			//go run tool/main.go --start "2019-11-13 17:44:25.302" --end "2019-11-13 18:44:27.302"
			&logparser.SsLineParser{"/Users/varadiadam/Downloads/logs/searchserver/tabsvc-stop-*.log"},

			// go run tool/main.go --start "2019-10-22 19:09:00.000" --end "2019-10-22 19:10:00.000"
			//&logparser.AcLineParser{"/Users/varadiadam/Downloads/logs/activemqserver/stdout_activemqserver_node1-0.*.log"},
		},
	}

	// Check times validity
	//start, err := time.Parse("2006-01-02T15:04:05", "2020-06-11T13:48:00.000")
	start, err := time.Parse("2006-01-02 15:04:05", *startStringRef)
	if err != nil {
		fmt.Println("Malformed start time:", err)
		os.Exit(-1)
	}

	end, err := time.Parse("2006-01-02 15:04:05", *endStringRef)
	if err != nil {
		fmt.Println("Malformed end time:", err)
		os.Exit(-1)
	}

	dur := *durationRef
	end, err = logparser.DurationParser(dur, start, end)
	if err != nil {
		fmt.Println("Malformed end time:", err)
		os.Exit(-1)
	}

	fmt.Println(start, end)

	logLines, err := parser.Process(start, end)

	if err != nil {
		panic(err)
	}

	for _, line := range logLines {

		if logparser.CheckIfTimeInBetween(start, end, line.TimeStamp) {
			fmt.Println(line)
		}
	}

}
